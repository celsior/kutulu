import sys
import math

# Survive the wrath of Kutulu
# Coded fearlessly by JohnnyYuge & nmahoude (ok we might have been a bit scared by the old god...but don't say anything)

INF = 9999999

width = int(input())
height = int(input())
map = []
for i in range(height):
    line = input()
    map.append(line)

dist = {}
for x in range(0,width):
    for y in range(0,height):
        if map[y][x] != '#':
            dist[(x,y)] = INF

def neighbors(cells):
    ret = []
    for x,y in cells:
        for c in [(x-1,y), (x+1, y), (x,y-1), (x, y+1)]:
            if c in dist and dist[c] == INF:
                ret.append(c)
    return ret

def calc_distances(init):
    depth = 0
    cells = [init]
    while cells:
        for c in cells:
            dist[c] = depth
        cells = neighbors(cells)
        depth += 1

def trace_back(pos, level, scent):
    scent[pos] = scent.get(pos, 0) + level
    x,y = pos
    for n in [(x-1,y), (x+1, y), (x,y-1), (x, y+1)]:
        if dist[pos] > 1:
            if dist.get(n, INF) == dist[pos]-1:
                trace_back(n, level*0.9, scent)
        else:
            if n in dist:
                scent[n] = scent.get(n, 0) + level*0.9


# sanity_loss_lonely: how much sanity you lose every turn when alone, always 3 until wood 1
# sanity_loss_group: how much sanity you lose every turn when near another player, always 1 until wood 1
# wanderer_spawn_time: how many turns the wanderer take to spawn, always 3 until wood 1
# wanderer_life_time: how many turns the wanderer is on map after spawning, always 40 until wood 1
sanity_loss_lonely, sanity_loss_group, wanderer_spawn_time, wanderer_life_time = [int(i) for i in input().split()]

# game loop
while True:
    wanderers = []
    coords = None
    entity_count = int(input())  # the first given entity corresponds to your explorer
    for i in range(entity_count):
        entity_type, id, x, y, param_0, param_1, param_2 = input().split()
        id = int(id)
        x = int(x)
        y = int(y)
        p0 = int(param_0)
        p1 = int(param_1)
        p2 = int(param_2)

        if coords == None:
            coords = (x,y)

        if entity_type == 'WANDERER':
            wanderers.append((x,y))

    for k in dist.keys():
        dist[k] = INF
    calc_distances(coords)

    scent = {}
    for w in wanderers:
        trace_back(w, 1.0, scent)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    # MOVE <x> <y> | WAIT
    x,y = coords
    best_pos = coords
    best_scent = scent.get(coords, 0)
    for n in [(x-1,y), (x+1, y), (x,y-1), (x, y+1)]:
        if n in dist and scent.get(n, 0) < best_scent:
            best_scent = scent.get(n, 0)
            best_pos = n

    print(f"MOVE {best_pos[0]} {best_pos[1]}")
